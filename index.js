const express = require("express");
const app = express();
const config = require("./src/config/config.js");
const rutaslibros = require("./src/router/router.js");
const puerto=config.server.puerto || process.env.PORT || 5000
app.use(express.json());
app.use("/api", rutaslibros);

app.listen(puerto, () => {
    console.log("servidor arriba")
});