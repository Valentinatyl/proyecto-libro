const mongoose = require("mongoose");
const librosSchema = new mongoose.Schema({

        autor: {
        type: String,
        required: true

        },
    
        fecha: {
        type: String,
        required: true
       },

        nombrelibro: {
        type: String,
        required: true,

    },
  
});

const Libros=mongoose.model("libros", librosSchema);
module.exports=Libros;
