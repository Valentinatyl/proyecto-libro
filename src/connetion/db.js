const mongo = require("mongoose");
const configdb = require("../config/config.js")
async function connectdb() {
    
    const mongourl = configdb.conecionbd.MONGO_URI;
    await mongo.connect(mongourl);
    console.log("Conectando a la base de datos Mongo")
}
module.exports = connectdb;