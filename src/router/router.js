const express = require("express");
const rutas = express.Router();

const libros = require("../models/librosModels.js")
const connectdb = require("../connetion/db.js");
connectdb();

rutas.get("/libros",async (req, res) => {  
    const traerlibros =await libros.find();
    res.json(traerlibros);
});

rutas.post("/libros",async (req, res) => {
    const { nombrelibro,autor, fecha  } = req.body
    const nuevoRegidtro = new libros({
        nombrelibro,
        autor,
        fecha
        
    }); 
    nuevoRegidtro.nombrelibro = nombrelibro;
    nuevoRegidtro.autor = autor;
    nuevoRegidtro.fecha = fecha;
    await nuevoRegidtro.save();
    res.json("Libro guardado");
});

rutas.put("/libros/:id",async (req, res) => {  
    const libroid = req.params.id;
    const {nombrelibro,autor,fecha} = req.body;
    const libroencontrado = await libros.findById(libroid);
    libroencontrado.nombrelibro = nombrelibro;
    libroencontrado.autor = autor;
    libroencontrado.fecha = fecha;
    

    await libroencontrado.save();
    res.json(libroencontrado)
});
rutas.delete("/libros/:id", async(req, res) => {  
    const libroid = req.params.id;
    const librobuscado = await libros.findById(libroid);
   await librobuscado.deleteOne();
    res.json("Libro eliminado")
});
module.exports = rutas;